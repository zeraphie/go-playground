package main

import (
	"fmt"
	"./src/page"
)

// Comments are php equivalents?

func main() {
    // $p1 = new Page({filename: 'test-page', body: parse('This is a sample page.')});
    p1 := &page.Page{FileName: "test-page", Body: []byte("This is a sample Page.")}

    // $p1.save();
    p1.Save()

    // $p2 = Page::load('test-page');
    p2, _ := page.Load("test-page")

    // echo json_encode($p2.body);
    fmt.Println(string(p2.Body))
}
