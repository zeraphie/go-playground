package page

import (
    "os"
    "io/ioutil"
)

const extension = ".txt"
const directory = "public/page-store"

type Page struct {
    FileName string
    Body  []byte
}

// Save the page
func (p *Page) Save() error {
    makeStoreIfNotExist()

    return saveToStore(p.FileName, p.Body)
}

// Load the page
func Load(filename string) (*Page, error) {
    body, err := loadFromStore(filename)

    // Throw an error
    if err != nil {
        return nil, err
    }

    return &Page{FileName: filename, Body: body}, nil
}

// Make the store if it doesn't already exist so that the save
// to store works
func makeStoreIfNotExist() {
    if _, err := os.Stat(directory); os.IsNotExist(err) {
        os.Mkdir(directory, os.ModePerm)
    }
}

// Save the data inside the store with the key of key
func saveToStore(key string, data []byte) error {
    return ioutil.WriteFile(directory + "/" + key + extension, data, 0600)
}

// Load the data from the store by the given key
func loadFromStore(key string) ([]byte, error) {
    return ioutil.ReadFile(directory + "/" + key + extension)
}