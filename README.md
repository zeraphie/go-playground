# GO Playground
This is meant to be a simple project to play around with GO

## Usage
```bash
# Go to project root and run in order to compile the project
go build -o public/main.exe app/main.go

# To run the project in windows, simply do
public\main
```